class Person(object):
  def __init__(self, name, age, height, weight):
    self.name = name
    self.age = age
    self.height = height
    self.weight = weight
    
  def __eq__(self, other):
    return self.name == other.name
          