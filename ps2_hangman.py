# 6.00 Problem Set 3
# 
# Hangman
#


# -----------------------------------
# Helper code
# (you don't need to understand this helper code)
import random
import string

WORDLIST_FILENAME = "words.txt"

def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    #print "Loading word list from file..."
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r', 0)
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = string.split(line)
    #print "  ", len(wordlist), "words loaded."
    return wordlist

def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code
# -----------------------------------

# actually load the dictionary of words and point to it with 
# the wordlist variable so that it can be accessed from anywhere
# in the program
wordlist = load_words()

gameword = []
# your code begins here!
def fill_guess_letters(word, letter):
  i = 0
  fill = ""
  while i < len(word):
    if word[i] == letter:
      gameword[i] = word[i]
    i += 1  
  for j in gameword:
    if j != " ":
      fill = fill + " " + j + " "
    else:
      fill = fill + " _ "
  return fill.strip()

def print_game_word(gameword):
  i = 0
  fill = ""
  while i < len(gameword):
    if gameword[i] != " ":
      fill = fill + " " + gameword[i] + " "
    else:
      fill = fill + " _ "
    i += 1  
  return fill.strip()  

def chek_equality(word, gameword):
  i = 0
  while i < len(word):
    if word[i] != gameword[i]:
      return False
    i += 1
  return True
    


print "Welcome to the game, Hangman!"
word = choose_word(wordlist)
#word = "abc"
print word
i = 0
while i < len(word):
  gameword.append(" ")
  i += 1
print "I am thinking of a word that is " + str(len(word)) + " letters long.-------------"

letters = "abcdefghijklmnopqrstuvwxyz"
num = 0
used_letters = ""
while num < 8:
  print "You have " + str( 8 - num) + " guesses left."
  print "Available letters: " + letters
  guess = raw_input("please guess a letter: ").lower()
  if word.find(guess) != -1 and used_letters.find(guess) == -1:
    print "Good guess: " + fill_guess_letters(word, guess)
    #used_letters = used_letters + guess
    letters = letters.replace(guess, "")
  else:
    print "Oops! That letter is not in my word: " + print_game_word(gameword) 
    #letters = letters.replace(guess, "")
    num += 1
  if (chek_equality(word, gameword)):
    print "You have won"
    break
  if num == 8:
    print "you have lost"
  letters = letters.replace(guess, "")




##Welcome to the game, Hangman!
##I am thinking of a word that is 4 letters long.-------------
##You have 8 guesses left.
##Available letters: abcdefghijklmnopqrstuvwxyz 
##Please guess a letter: a
##Good guess: _a _ _
##------------
##You have 8 guesses left.
##Available letters: bcdefghijklmnopqrstuvwxyz
##Please guess a letter: s
##Oops! That letter is not in my word: _a _ _------------
##You have 7 guesses left.
##Available letters: bcdefghijklmnopqrtuvwxyz
##Please guess a letter: t
##Good guess: ta _t
##------------



