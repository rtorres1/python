def evaluate_poly(poly, x):
  result = 0.0
  i=0
  for e in poly:
    result = result + e*x**i
    i += 1
  return  result

def compute_deriv(poly):
  result = ()
  for i in range(1, len(poly)):
    result = result + (poly[i]*i,)
  return result
  
def compute_root(poly, x_0, epsilon):
  poly_dev = compute_deriv(poly)
  count = 1
  
  while (abs(evaluate_poly(poly,x_0)) >= epsilon):
    count += 1
    x_0 = x_0 - evaluate_poly(poly, x_0)/evaluate_poly(poly_dev, x_0)
    
  return (x_0, count)  
    
    



poly = (0.0, 0.0, 5.0, 9.3, 7.0)
x = -13

poly1 = (-13.39, 0, 17.5, 3.0, 1.0)

print evaluate_poly(poly, x)

print compute_deriv(poly1)

print compute_root(poly1, 0.1, 0.001)