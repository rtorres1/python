# 6.00 Problem Set 4
#
# Caesar Cipher Skeleton
#
import string
import random

WORDLIST_FILENAME = "words.txt"

# -----------------------------------
# Helper code
# (you don't need to understand this helper code)
def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print ("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r', 0)
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print "  ", len(wordlist), "words loaded."
    return wordlist

wordlist = load_words()

def is_word(wordlist, word):
    """
    Determines if word is a valid word.

    wordlist: list of words in the dictionary.
    word: a possible word.
    returns True if word is in wordlist.

    Example:
    >>> is_word(wordlist, 'bat') returns
    True
    >>> is_word(wordlist, 'asdf') returns
    False
    """
    word = word.lower()
    #if len(word) > 0:
      #if word[0] == " ":
	#return False
    word = word.strip(" !@#$%^&*()-_+={}[]|\:;'<>?,./\"")
    return word in wordlist

def random_word(wordlist):
    """
    Returns a random word.

    wordlist: list of words  
    returns: a word from wordlist at random
    """
    return random.choice(wordlist)

def random_string(wordlist, n):
    """
    Returns a string containing n random words from wordlist

    wordlist: list of words
    returns: a string of random words separated by spaces.
    """
    return " ".join([random_word(wordlist) for _ in range(n)])

def random_scrambled(wordlist, n):
    """
    Generates a test string by generating an n-word random string
    and encrypting it with a sequence of random shifts.

    wordlist: list of words
    n: number of random words to generate and scamble
    returns: a scrambled string of n random words


    NOTE:
    This function will ONLY work once you have completed your
    implementation of apply_shifts!
    """
    s = random_string(wordlist, n) + " "
    shifts = [(i, random.randint(0, 26)) for i in range(len(s)) if s[i-1] == ' ']
    return apply_shifts(s, shifts)[:-1]

def get_fable_string():
    """
    Returns a fable in encrypted text.
    """
    f = open("fable.txt", "r")
    fable = str(f.read())
    f.close()
    return fable


# (end of helper code)
# -----------------------------------

#
# Problem 1: Encryption
#
def build_coder(shift):
  
    """
    Returns a dict that can apply a Caesar cipher to a letter.
    The cipher is defined by the shift value. Ignores non-letter characters
    like punctuation and numbers.

    shift: -27 < int < 27
    returns: dict

    Example:
    >>> build_coder(3)
    {' ': 'c', 'A': 'D', 'C': 'F', 'B': 'E', 'E': 'H', 'D': 'G', 'G': 'J',
    'F': 'I', 'I': 'L', 'H': 'K', 'K': 'N', 'J': 'M', 'M': 'P', 'L': 'O',
    'O': 'R', 'N': 'Q', 'Q': 'T', 'P': 'S', 'S': 'V', 'R': 'U', 'U': 'X',
    'T': 'W', 'W': 'Z', 'V': 'Y', 'Y': 'A', 'X': ' ', 'Z': 'B', 'a': 'd',
    'c': 'f', 'b': 'e', 'e': 'h', 'd': 'g', 'g': 'j', 'f': 'i', 'i': 'l',
    'h': 'k', 'k': 'n', 'j': 'm', 'm': 'p', 'l': 'o', 'o': 'r', 'n': 'q',
    'q': 't', 'p': 's', 's': 'v', 'r': 'u', 'u': 'x', 't': 'w', 'w': 'z',
    'v': 'y', 'y': 'a', 'x': ' ', 'z': 'b'}
    (The order of the key-value pairs may be different.)
    """
    ### TODO.
    try:
      shift = int(shift)
      if shift <-26 or shift > 26:
	print "An integer between -26 <= shift <= 26 should be enter"
	return
    except ValueError:
      print "You must enter an integer value"
      return 
	
    alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',' ']
    if shift < 0:
      shift = len(alphabet) + shift
    dictionary = {}
    for i in xrange(0,len(alphabet)):
      dictionary[alphabet[i]] = alphabet[(i+shift)%27]
      dictionary[alphabet[i].lower()] = alphabet[(i+shift)%27].lower()
    #print dictionary
    return dictionary
    
def build_encoder(shift):
    """
    Returns a dict that can be used to encode a plain text. For example, you
    could encrypt the plain text by calling the following commands
    >>>encoder = build_encoder(shift)
    >>>encrypted_text = apply_coder(plain_text, encoder)
    
    The cipher is defined by the shift value. Ignores non-letter characters
    like punctuation and numbers. 

    shift: 0 <= int < 27
    returns: dict

    Example:
    >>> build_encoder(3)
    {' ': 'c', 'A': 'D', 'C': 'F', 'B': 'E', 'E': 'H', 'D': 'G', 'G': 'J',
    'F': 'I', 'I': 'L', 'H': 'K', 'K': 'N', 'J': 'M', 'M': 'P', 'L': 'O',
    'O': 'R', 'N': 'Q', 'Q': 'T', 'P': 'S', 'S': 'V', 'R': 'U', 'U': 'X',
    'T': 'W', 'W': 'Z', 'V': 'Y', 'Y': 'A', 'X': ' ', 'Z': 'B', 'a': 'd',
    'c': 'f', 'b': 'e', 'e': 'h', 'd': 'g', 'g': 'j', 'f': 'i', 'i': 'l',
    'h': 'k', 'k': 'n', 'j': 'm', 'm': 'p', 'l': 'o', 'o': 'r', 'n': 'q',
    'q': 't', 'p': 's', 's': 'v', 'r': 'u', 'u': 'x', 't': 'w', 'w': 'z',
    'v': 'y', 'y': 'a', 'x': ' ', 'z': 'b'}
    (The order of the key-value pairs may be different.)

    HINT : Use build_coder.
    """
    ### TODO.
    if shift == 27:
      shift = 0
    try:
      shift = int(shift)
      if shift < 0 or shift > 26:
	print "An integer between 0 <= shift <= 26 should be enter"
	return
    except ValueError:
      print "You must enter an integer value"
      return 
    #print  build_coder(shift)
    return build_coder(shift)

def build_decoder(shift):
    """
    Returns a dict that can be used to decode an encrypted text. For example, you
    could decrypt an encrypted text by calling the following commands
    >>>encoder = build_encoder(shift)
    >>>encrypted_text = apply_coder(plain_text, encoder)
    >>>decrypted_text = apply_coder(plain_text, decoder)
    
    The cipher is defined by the shift value. Ignores non-letter characters
    like punctuation and numbers.

    shift: 0 <= int < 27
    returns: dict

    Example:
    >>> build_decoder(3)
    {' ': 'x', 'A': 'Y', 'C': ' ', 'B': 'Z', 'E': 'B', 'D': 'A', 'G': 'D',
    'F': 'C', 'I': 'F', 'H': 'E', 'K': 'H', 'J': 'G', 'M': 'J', 'L': 'I',
    'O': 'L', 'N': 'K', 'Q': 'N', 'P': 'M', 'S': 'P', 'R': 'O', 'U': 'R',
    'T': 'Q', 'W': 'T', 'V': 'S', 'Y': 'V', 'X': 'U', 'Z': 'W', 'a': 'y',
    'c': ' ', 'b': 'z', 'e': 'b', 'd': 'a', 'g': 'd', 'f': 'c', 'i': 'f',
    'h': 'e', 'k': 'h', 'j': 'g', 'm': 'j', 'l': 'i', 'o': 'l', 'n': 'k',
    'q': 'n', 'p': 'm', 's': 'p', 'r': 'o', 'u': 'r', 't': 'q', 'w': 't',
    'v': 's', 'y': 'v', 'x': 'u', 'z': 'w'}
    (The order of the key-value pairs may be different.)

    HINT : Use build_coder.
    """
    ### TODO.
    if shift == 27:
      shift = 0
    try:
      shift = int(shift)
      if shift < 0 or shift > 26:
	print "An integer between 0 <= shift <= 26 should be enter"
	return
    except ValueError:
      print "You must enter an integer value"
      return 
    #print  build_coder(shift)
    shift = -shift
    return build_coder(shift)
 

def apply_coder(text, coder):
    """
    Applies the coder to the text. Returns the encoded text.

    text: string
    coder: dict with mappings of characters to shifted characters
    returns: text after mapping coder chars to original text

    Example:
    >>> apply_coder("Hello, world!", build_encoder(3))
    'Khoor,czruog!'
    >>> apply_coder("Khoor,czruog!", build_decoder(3))
    'Hello, world!'
    """
    ### TODO.
    coded_text = []
    for c in text:
      if c in coder.keys():
	coded_text.append(coder[c])
      else:
	coded_text.append(c)
    return "".join(coded_text)
  

def apply_shift(text, shift):
    """
    Given a text, returns a new text Caesar shifted by the given shift
    offset. The empty space counts as the 27th letter of the alphabet,
    so spaces should be replaced by a lowercase letter as appropriate.
    Otherwise, lower case letters should remain lower case, upper case
    letters should remain upper case, and all other punctuation should
    stay as it is.
    
    text: string to apply the shift to
    shift: amount to shift the text
    returns: text after being shifted by specified amount.

    Example:
    >>> apply_shift('This is a test.', 8)
    'Apq hq hiham a.'
    """
    ### TODO.
    return apply_coder(text, build_encoder(shift))
   
#
# Problem 2: Codebreaking.
#
def find_best_shift(wordlist, text):
    """
    Decrypts the encoded text and returns the plaintext.

    text: string
    returns: 0 <= int 27

    Example:
    >>> s = apply_coder('Hello, world!', build_encoder(8))
    >>> s
    'Pmttw,hdwztl!'
    >>> find_best_shift(wordlist, s) returns
    8
    >>> apply_coder(s, build_decoder(8)) returns
    'Hello, world!'
    """
    ### TODO
    decrypted_text = []
    number_of_words = 0
    shift_value = 0
    for i in xrange(27):
      s = apply_coder(text, build_decoder(i))
      if not s[0] == " ":
	transitory_array1 = s.split(" ")
	if is_word(wordlist, transitory_array1[0]):
	  transitory_array2 = []
	  for j in range(0, len(transitory_array1)):
	    if is_word(wordlist, transitory_array1[j]):
	      transitory_array2.append(transitory_array1[j] + " ")		  
	  if len(transitory_array2) > len(decrypted_text):
	    decrypted_text = transitory_array2
	    shift_value = i
    
    return shift_value
#
# Problem 3: Multi-level encryption.
#
def apply_shifts(text, shifts):
    """
    Applies a sequence of shifts to an input text.

    text: A string to apply the Ceasar shifts to 
    shifts: A list of tuples containing the location each shift should
    begin and the shift offset. Each tuple is of the form (location,
    shift) The shifts are layered: each one is applied from its
    starting position all the way through the end of the string.  
    returns: text after applying the shifts to the appropriate
    positions

    Example:
    >>> apply_shifts("Do Androids Dream of Electric Sheep?", [(0,6), (3, 18), (12, 16)])
    'JufYkaolfapxQdrnzmasmRyrpfdvpmEurrb?'
    """
    ### TODO.
    
    result = text[:]
    for i in xrange(len(shifts)):
      position = shifts[i][0]
      text1 = result[position:]
      text1 = apply_shift(text1,shifts[i][1])
      if position > 0:
	result = result[0:position] + text1
      else:
	result = text1[:]
      
    return result  
#
# Problem 4: Multi-level decryption.
#


#def find_best_shifts(wordlist, text):
    #"""
    #Given a scrambled string, returns a shift key that will decode the text to
    #words in wordlist, or None if there is no such key.

    #Hint: Make use of the recursive function
    #find_best_shifts_rec(wordlist, text, start)

    #wordlist: list of words
    #text: scambled text to try to find the words for
    #returns: list of tuples.  each tuple is (position in text, amount of shift)
    
    #Examples:
    #>>> s = random_scrambled(wordlist, 3)
    #>>> s
    #'eqorqukvqtbmultiform wyy ion'
    #>>> shifts = find_best_shifts(wordlist, s)
    #>>> shifts
    #[(0, 25), (11, 2), (21, 5)]
    #>>> apply_shifts(s, shifts)
    #'compositor multiform accents'
    #>>> s = apply_shifts("Do Androids Dream of Electric Sheep?", [(0,6), (3, 18), (12, 16)])
    #>>> s
    #'JufYkaolfapxQdrnzmasmRyrpfdvpmEurrb?'
    #>>> shifts = find_best_shifts(wordlist, s)
    #>>> print apply_shifts(s, shifts)
    #Do Androids Dream of Electric Sheep?
    #"""
    #s = ""
    #text1 = text[:]
    #shift = 0
    #position = 0
    #list_tupples = []
    #transition = ""
    #previous_shift = 0
    #s0 = ""
    #chek = False
    #chek1 = True
    
    #for i in xrange(1,len(text)-1):
      #s = text1[0:i+1-position]
      #s0 = text1[0:i+1-position]
      #shift = find_best_shift(wordlist,s)
      #s = apply_coder(s, build_decoder(shift))
      #s0 = apply_coder(s0, build_decoder(previous_shift))
      #print "This is s : ", s, "--its lenght is:", len(s), "Shift is:", shift
      #print "This is s0: ", s0, "--its lenght is:", len(s0), "Previous Shift is:", previous_shift
      ##This part of the code checks if there is a better word that has lenght greater than 1
      #if len(s) > 1:
	#if is_word(wordlist, s[0]) and s[1] == " ":
	  #for j in xrange(27):
	    #if not j == shift:
	      #s1 = apply_coder(text[position:], build_decoder(j))
	      #if is_word(wordlist, s1[0:2]) and s1[2] == " ":
		#s = s1[0:3]
		#s0 = s1
		#shift = j
		##i = i + 1
		#break
	#if is_word(wordlist, s[0]) and s[1] == " ":
	  #chek = True
	  
      
      #if (is_word(wordlist,s[0:-1]) and s[-1] == " ") or (is_word(wordlist,s0[0:-1]) and s0[-1] == " ") or is_word(wordlist, apply_coder(text[position:], build_decoder(shift))) or chek:
	#if (is_word(wordlist,s0[0:-1]) and s0[-1] == " "):
	  #s = s0
	  #shift = previous_shift
	#if not shift == 0 and not s[0] == " ":
	  #list_tupples.append((position, 27-shift))
	  #if chek:
	    #position += 2
	  #else:  
	    #position = i + 1
	  #text = apply_coder(text, build_decoder(shift))
	#shift = 0
	#previous_shift = 0
	#chek = False
	#text1 = text[position:]  
	#transitory_array1 = text1.split()
	#print transitory_array1
	#print position
	#for i in transitory_array1:
	  #if is_word(wordlist, i):
	    #position += len(i) + 1
	    #print "Value of position", position
	    #if position >= len(text):
	      #return list_tupples
	  #else:
	    #break
	#text1 = text[position:]  
	#print text1
	
      #previous_shift = shift
    #return list_tupples
      
      

#def find_best_shifts_rec(wordlist, text, start):
    #"""
    #Given a scrambled string and a starting position from which
    #to decode, returns a shift key that will decode the text to
    #words in wordlist, or None if there is no such key.

    #Hint: You will find this function much easier to implement
    #if you use recursion.

    #wordlist: list of words
    #text: scambled text to try to find the words for
    #start: where to start looking at shifts
    #returns: list of tuples.  each tuple is (position in text, amount of shift)
    #"""
    #### TODO.



############################################################################################################################################################
############################################################################################################################################################
def find_best_shifts(wordlist, text):
    
    return find_best_shifts_rec(wordlist, text, 0)


def find_spaces(text, start):
    """
    given a text and start position, find all the spaces
    returns a list of all the indices. empty list if none
    """
    tally = []
    tmptext = text[start:]
    counter = 0
    for i in tmptext:
        if i == " ":
            tally.append(counter+start)
        counter+=1
    return tally


def is_tupleorlist(arg1):
    """
    checks if something is a list or a tuple
    """
    if type(arg1) == tuple or type(arg1) == list:
        return True
    else:
        return False

def find_best_shifts_rec(wordlist, text, start):
    """
    Given a scrambled string and a starting position from which
    to decode, returns a shift key that will decode the text to
    words in wordlist, or None if there is no such key.
    wordlist: list of words
    text: scambled text to try to find the words for
    start: where to start looking at shifts
    returns: list of tuples.  each tuple is (position in text, amount of shift)
    """

    """for every possible shift from 0 to 27"""
    for n in range (0,27):

        answer = []
        curr_shift_tuple = start,27-n

        """set s to be the text up to the start parameter concatenated with the text
        after the start parameter that has been shifted by n"""
        s = text[:start] + apply_coder(text[start:], build_decoder(n))

        """look for the spaces beginning at the start location"""
        spaceslist = find_spaces(s, start)

        """if we find a space and the point from start to first space are a valid word then we
        recurse the function starting at the point of the space
        There's only 2 possible outcomes from doing our shift. Either there's a space in the shifted
        chunk or there isn't, which means we should test of that chunk is a word or not (we're at the
        end of the sentence possibly)
        """
        if spaceslist != []:
            j = s[start:spaceslist[0]] #j is the first word from start
            if is_word(wordlist, j): #check if j is a word
                foo = find_best_shifts_rec(wordlist, s, spaceslist[0]+1)
                if is_tupleorlist(foo): #if the recursive call returns a tuple then we've found an answer
                    answer.append(curr_shift_tuple)
                    answer += foo
                    return answer
                    #if foo is not a tuple then do nothing and continue checking range
        elif spaceslist == []:
            j = s[start:len(s)] #j is the string from start to the end of s
            if is_word(wordlist, j): #check if j is a word
                answer.append(curr_shift_tuple)
                return answer

    return None


#foo2 = "dnbxivmcmzuwzmhuebgsfdhsnyhepvwumpikzlydi p aivl hfgesffsfnglqqc eyryofdeiziqjz xk"
#bar = find_best_shifts(wordlist, foo2)
#print bar
#print apply_shifts(foo2, bar)
#s = "tlsrfcyxlkejpa wsusktz"
#bar = find_best_shifts(wordlist,s)
#print bar
bar = [(0, 27), (3, 15), (13, 26), (17, 22), (21, 23), (25, 5), (31, 22), (33, 23), (40, 1), (48, 2), (56, 13), (58, 14), (64, 24), (74, 5), (77, 11), (84, 9), (87, 14), (91, 25), (94, 12), (97, 3), (101, 8), (104, 21), (108, 2), (118, 17), (126, 2), (137, 14), (143, 10), (150, 7), (153, 18), (161, 17), (165, 17), (169, 20), (173, 19), (180, 25), (182, 15), (184, 20), (187, 16), (194, 18), (198, 21), (206, 23), (218, 14), (224, 17), (232, 15), (236, 23), (244, 9), (257, 9), (262, 6), (268, 18), (271, 22), (275, 6), (284, 19), (288, 19), (293, 20), (297, 8), (300, 6), (306, 25), (311, 5), (315, 26), (322, 3), (326, 8), (335, 4), (345, 16), (349, 22), (356, 8), (359, 15), (364, 17), (367, 24), (372, 2), (381, 20), (389, 21), (394, 20), (398, 8), (401, 12), (406, 8), (411, 16), (418, 11), (421, 27), (433, 7), (437, 4), (449, 10), (452, 2), (455, 13), (464, 17), (468, 19), (478, 2), (481, 17), (488, 21), (493, 17), (495, 27), (499, 19), (503, 25), (507, 16), (514, 4), (520, 11), (526, 9), (531, 8), (538, 18), (544, 2), (548, 19), (562, 19), (567, 15), (572, 23), (582, 19), (586, 19), (593, 23), (598, 27), (601, 18), (606, 3), (611, 14), (625, 9), (628, 12), (634, 21), (636, 15), (643, 27)]
print apply_shifts(get_fable_string(), bar)