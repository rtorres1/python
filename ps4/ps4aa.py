def find_best_shifts_rec(wordlist, text, start):
    """
    Given a scrambled string and a starting position from which
    to decode, returns a shift key that will decode the text to
    words in wordlist, or None if there is no such key.

    Hint: You will find this function much easier to implement
    if you use recursion.

    wordlist: list of words
    text: scambled text to try to find the words for
    start: where to start looking at shifts
    returns: list of tuples.  each tuple is (position in text, amount of shift)
    """
    old_start=start
    max_len_of_word = 0 ###
    shift = -1          ###
    word = ""           ###
    if start > len(text):
        print 'hit end'
        return []
    else:
        ##for i in range(0,28):
            ##words=apply_coder(text[old_start:],build_decoder(i)).split(' ')
            ##if is_word(wordlist,words[0]):
                ##text1 = text[:old_start]+apply_coder(text[old_start:],build_decoder(i))
                ##ans=find_best_shifts_rec(wordlist,text1,start+len(words[0])+1)
                ##if ans is not None:
                    ##return [(old_start,-i)] + ans
        ##return None 
        for i in range(0,28):
	  words=apply_coder(text[old_start:],build_decoder(i)).split(' ')
	  if is_word(wordlist,words[0]):
	    if len(words[0]) > max_len_of_word:
	      max_len_of_word = len(words[0])
	      word = words[0] 
	      shift = i
	if shift > -1:
	  text1 = text[:old_start]+apply_coder(text[old_start:],build_decoder(shift))
	  ans=find_best_shifts_rec(wordlist,text1,start+len(word)+1)
	  if ans is not None:
	    return [(old_start,-shift)] + ans
	return None  