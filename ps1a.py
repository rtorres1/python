balance = float(raw_input("Enter the outstanding balance on your credit card: $"))

ir =  float(raw_input("Enter the annual credit card interest rate as a decimal: "))

mr = float(raw_input("Enter the minimum monthly payment rate as a decimal: "))

total_paid = 0

for month in range(1,13):
  print "Month: ", month
  print "Minimum montly payment: ", round(balance*mr,2)
  total_paid += round(balance*mr,2)
  principal_paid = round(balance*mr,2) - round(ir/12*balance,2)
  print "Principle paid: $", principal_paid
  balance = round(balance - principal_paid,2)
  print "Remaining balance: $", balance
print "RESULT"
print "Total amount paid: $", total_paid
print "Remaining balance: $", balance
  